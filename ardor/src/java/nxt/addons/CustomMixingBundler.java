package nxt.addons;

import nxt.ae.AssetExchangeTransactionType;
import nxt.blockchain.Bundler;
import nxt.blockchain.ChildChain;
import nxt.blockchain.ChildTransaction;
import nxt.blockchain.TransactionType;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.*;

public class CustomMixingBundler implements Bundler.Filter {

    private class BundleRule {
        private int type, subtype;
        private boolean messagePrunableAllowed = true;
        private boolean messagePrunableRequired = false;
        private long maxFeeFQT = 3000000;

        private HashSet<Long> ids = new HashSet<>();
        private HashSet<Long> senders = new HashSet<>();

        public BundleRule(int type, int subtype) {
            this.type = type;
            this.subtype  = subtype;
        }

        public void addId(long id) {
            ids.add(id);
        }
    }

    private static class Configuration {
        private final String fileName = "conf/processes/CustomMixingBundler.json";
        FileTime fileTimeModified = null;

        int height = 0;

        List<Integer> chain;

        String adminPasswordString = null;

        List<BundleRule> rules;

        boolean mixEnabled = true;

        double minFeeRatePerFXT = 3.333;

        private Configuration() {
        }
    }

    private static final CustomMixingBundler.Configuration configuration = new CustomMixingBundler.Configuration();

    private void setup(){
        File file;

        try {
            file = new File(configuration.fileName);
        } catch (Exception e) {
            return;
        }

        long fileLength = file.length();

        if (fileLength == 0) {
            return;
        }

        Path path = file.toPath();
        BasicFileAttributes basicFileAttributes;

        try {
            basicFileAttributes = Files.readAttributes(path, BasicFileAttributes.class);
        } catch (IOException e) {
            return;
        }

        FileTime fileTimeModified = basicFileAttributes.lastModifiedTime();

        if(configuration.fileTimeModified != null && configuration.fileTimeModified.equals(fileTimeModified)) {
            return;
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonConfiguration;

        try {
            jsonConfiguration = (JSONObject) jsonParser.parse(new String (Files.readAllBytes(file.toPath())));
        } catch (ParseException | IOException e) {
            return;
        }

        if(jsonConfiguration == null) {
            return;
        }

        configuration.chain  = new ArrayList<>();
        configuration.rules = new ArrayList<>();

        if(jsonConfiguration.containsKey("chainId")) {
            JSONArray arrayIds = (JSONArray) jsonConfiguration.get("chainId");
            SortedSet<Long> list = JsonSimpleFunctions.listLongFromJsonStringArray(arrayIds);

            for(long id: list) {
             configuration.chain.add((int) id);
            }
        } else {
            configuration.chain.add(2);
        }

        if(jsonConfiguration.containsKey("any")) {
            JSONObject object = (JSONObject) jsonConfiguration.get("any");

            configuration.mixEnabled = JsonSimpleFunctions.getBoolean(object, "enabled", configuration.mixEnabled);
            configuration.minFeeRatePerFXT = JsonSimpleFunctions.getDouble(object, "minFeeRatePerFXT", configuration.minFeeRatePerFXT);
        }

        if(jsonConfiguration.containsKey("rules")) {
            JSONArray array = (JSONArray) jsonConfiguration.get("rules");

            for (Object object: array) {
                JSONObject ruleJSON = (JSONObject) object;

                int type = JsonSimpleFunctions.getInt(ruleJSON, "type", -1);
                int subtype = JsonSimpleFunctions.getInt(ruleJSON, "subtype", -1);

                if(type < 0 || subtype < 0) {
                    continue;
                }

                BundleRule rule = new BundleRule(type, subtype);

                if(ruleJSON.containsKey("messagePrunable")) {
                    JSONObject attachmentConfiguration = (JSONObject) ruleJSON.get("messagePrunable");
                    rule.messagePrunableAllowed = JsonSimpleFunctions.getBoolean(attachmentConfiguration, "allowed", rule.messagePrunableAllowed);
                    rule.messagePrunableRequired = JsonSimpleFunctions.getBoolean(attachmentConfiguration, "ifAllowedRequire", rule.messagePrunableRequired);
                }

                rule.maxFeeFQT = JsonSimpleFunctions.getLongFromStringUnsigned(ruleJSON, "maxFeeFQT", rule.maxFeeFQT);

                if(ruleJSON.containsKey("ids")) {
                    JSONArray arrayIds = (JSONArray) ruleJSON.get("ids");
                    SortedSet<Long> list = JsonSimpleFunctions.listLongFromJsonStringArray(arrayIds);

                    for (long id: list) {
                        rule.ids.add(id);
                    }
                }

                if(ruleJSON.containsKey("senders")) {
                    JSONArray sendersJSON = (JSONArray) ruleJSON.get("senders");
                    SortedSet<Long> list = JsonSimpleFunctions.listLongFromJsonStringArray(sendersJSON);

                    for (long id: list) {
                        rule.senders.add(id);
                    }
                }

                configuration.rules.add(rule);
            }
        }

        configuration.height = getBlockHieght();
        configuration.fileTimeModified = fileTimeModified;
    }

    @Override
    public boolean ok(Bundler bundler, ChildTransaction childTransaction) {
        int height =  getBlockHieght();

        synchronized (configuration) {
            if (configuration.height < height) {
                setup();
            }
        }

        ChildChain chain = childTransaction.getChain();

        if(!configuration.chain.contains(chain.getId())) {
            return false;
        }

        long minimumFeeFQT = childTransaction.getMinimumFeeFQT();

        if(configuration.mixEnabled) {
            double rate = ((double)childTransaction.getFee() / (double)chain.ONE_COIN) / ((double)minimumFeeFQT / 100000000.0);

            if (rate >= configuration.minFeeRatePerFXT) {
                return true;
            }
        }


        boolean isValid = false;

        for(BundleRule rule : configuration.rules) {
            if(! rule.senders.contains(childTransaction.getSenderId())) {
                continue;
            }

            if(minimumFeeFQT > rule.maxFeeFQT) {
                continue;
            }

            if(!rule.messagePrunableAllowed && childTransaction.getPrunablePlainMessage() != null) {
                continue;
            } else if(rule.messagePrunableRequired && childTransaction.getPrunablePlainMessage() == null) {
                continue;
            }

            TransactionType type = childTransaction.getType();

            if(type.getType() != rule.type || type.getSubtype() != rule.subtype) {
                continue;
            }

            switch (rule.type) {
                case 2: {
                    long assetId = ((AssetExchangeTransactionType) type).getAssetId(childTransaction);

                    if(rule.ids.isEmpty() || rule.ids.contains(assetId)) {
                        isValid = true;
                    }

                    break;
                }
            }

            if(isValid) {
                break;
            }
        }

        return isValid;
    }

    public int getBlockHieght() {
        return nxt.http.callers.GetBlockchainStatusCall.create().call().getInt("numberOfBlocks") - 1;
    }

    public static class JsonSimpleFunctions {

        public static boolean getBoolean(JSONObject jsonObject, String key, boolean defaultValue) {
            boolean result = defaultValue;

            if(jsonObject != null) {
                if(jsonObject.containsKey(key)) {
                    result = (boolean) jsonObject.get(key);
                }
            }

            return result;
        }

        public static int getInt(JSONObject jsonObject, String key, int defaultValue) {
            long result = defaultValue;

            if(jsonObject != null) {
                if(jsonObject.containsKey(key)) {
                    result = (long) jsonObject.get(key);
                }
            }

            return (int) result;
        }

        public static long getLong(JSONObject jsonObject, String key, int defaultValue) {
            long result = defaultValue;

            if(jsonObject != null) {
                if(jsonObject.containsKey(key)) {
                    result = (long) jsonObject.get(key);
                }
            }

            return result;
        }

        public static double getDouble(JSONObject jsonObject, String key, double defaultValue) {
            double result = defaultValue;

            if(jsonObject != null) {
                if(jsonObject.containsKey(key)) {
                    result = (double) jsonObject.get(key);
                }
            }

            return result;
        }

        public static long getLongFromStringUnsigned(JSONObject jsonObject, String key, long defaultValue) {
            long result = defaultValue;

            if(jsonObject != null) {
                if(jsonObject.containsKey(key)) {
                    result = Long.parseUnsignedLong((String) jsonObject.get(key));
                }
            }

            return result;
        }

        public static SortedSet<Long> listLongFromJsonStringArray(JSONArray jsonStringArray) {

            int jsonArraySize = jsonStringArray.size();

            if( jsonArraySize == 0)
                return null;

            SortedSet<Long> list = new TreeSet<>();

            for(int i = 0; i < jsonArraySize; i++) {
                list.add(Long.parseUnsignedLong(jsonStringArray.get(i).toString()));
            }

            return list;
        }
    }

    @Override
    public String getName() {
        return "CustomMixingBundler";
    }

    @Override
    public String getDescription() {
        return "Custom rules auto-loaded via JSON; Mixes with general transactions";
    }
}
